import { Component, OnInit } from '@angular/core';
import {StudentsElement} from '../../interfaces/interfaces_main';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.css']
})
export class OneComponent implements OnInit {

  columnHeader = {studentID: 'ID', fname: 'First Name', lname: 'Last Name', weight: 'Weight', symbol: 'Code', action: 'abc'};
  tableData: StudentsElement[] = [
    {studentID: 1, fname: 'Hydrogen', weight: 1.0079, symbol: 'H', action: [
      {delete: true}, {edit: true}, {add: true}, {public: true}, {private: true}
    ] },
    {studentID: 2, fname: 'Helium', weight: 4.0026, symbol: 'He', action: [{delete: true}, {edit: true}, {add: false}] },
    {studentID: 3, fname: 'Lithium', weight: 6.941, symbol: 'Li', action: [{delete: true}, {edit: true}, {add: false}] },
    {studentID: 4, fname: 'Beryllium', weight: 9.0122, symbol: 'Be', action: [{delete: true}, {edit: true}, {add: false}] },
    {studentID: 5, fname: 'Boron', weight: 10.811, symbol: 'B', action: [{delete: true}, {edit: true}, {add: false}] },
    {studentID: 6, fname: 'Carbon', weight: 12.0107, symbol: 'C', action: [{delete: true}, {edit: true}, {add: false}] },
    {studentID: 7, fname: 'Nitrogen', weight: 14.0067, symbol: 'N', action: [{delete: true}, {edit: true}, {add: false}] },
    {studentID: 8, fname: 'Oxygen', weight: 15.9994, symbol: 'O', action: [{delete: true}, {edit: true}, {add: false}] },
    {studentID: 9, fname: 'Fluorine', weight: 18.9984, symbol: 'F', action: [{delete: true}, {edit: true}, {add: false}] },
    {studentID: 10, fname: 'Neon', weight: 20.1797, symbol: 'Ne', action: [{delete: true}, {edit: true}, {add: false}] },
  ];


  constructor() { }

  ngOnInit() {
  }

  takeAction(actionWithData) {
    console.log('action Item :' , actionWithData.actionItem, 'rowData :' , actionWithData.rowData);
  }



}
