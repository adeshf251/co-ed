import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Sample1RoutingModule } from './sample1-routing.module';
import { OneComponent } from './one/one.component';
import { SharedModule } from '../shared/shared.module';
import { Sample2Module } from '../sample2/sample2.module';
import { TwoComponent } from './two/two.component';


@NgModule({
  declarations: [OneComponent, TwoComponent],
  imports: [
    CommonModule,
    Sample1RoutingModule,
    SharedModule,
    Sample2Module
  ]
})
export class Sample1Module { }
