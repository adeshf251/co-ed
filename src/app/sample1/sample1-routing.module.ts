import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';


const routes: Routes = [
  {
    path: 'skjcnaksc',
    component: OneComponent
  },
  {
    path: '',
    component: TwoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Sample1RoutingModule { }
