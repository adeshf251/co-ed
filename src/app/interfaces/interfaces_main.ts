export interface StudentsElement {
  fname: string;
  studentID: number;
  weight: number;
  symbol: string;
  action: any;
}
