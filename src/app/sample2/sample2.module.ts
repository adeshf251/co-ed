import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnackBarComponent } from './snack-bar/snack-bar.component';
import {
  DialogComponent,
  DialogContentExampleDialogComponent
} from './dialog/dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule, MatButtonToggleModule } from '@angular/material';

@NgModule({
  entryComponents: [DialogComponent, DialogContentExampleDialogComponent],
  declarations: [
    SnackBarComponent,
    DialogComponent,
    DialogContentExampleDialogComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatButtonToggleModule
  ],
  exports: [DialogComponent, SnackBarComponent]
})
export class Sample2Module {}
