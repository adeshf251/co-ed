import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  @Output() actionTriggered = new EventEmitter();
  @Input() dialogTitle;
  @Input() dialogMsg;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  openDialog() {
    const dialogRef = this.dialog.open(DialogContentExampleDialogComponent);
    // const sub = dialogRef.componentInstance.onOkayButtonClicked.subscribe(() => {
    //   // do something
    //   console.log('okay Clicked');
    // });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result === true) {
        this.actionTriggered.emit(true);
      } else {
        this.actionTriggered.emit(false);
      }
    });

    // let config = new MdDialogConfig();
    // let dialogRef:MdDialogRef<PizzaDialog> = this.dialog.open(PizzaDialog, config);
    dialogRef.componentInstance.dialogTitle = this.dialogTitle;
    dialogRef.componentInstance.messageBody = this.dialogMsg;
  }

  printData(actionItem, rowData) {
    console.log(actionItem, rowData);
    this.actionTriggered.emit({actionItem, rowData});
  }
}

@Component({
  selector: 'app-dialog-content-example-dialog',
  templateUrl: 'dialog-content-example-dialog.html'
})
export class DialogContentExampleDialogComponent {
  // onOkayButtonClicked = new EventEmitter();
  dialogTitle: string;
  messageBody: string;

  // onButtonClick() {
  //   this.onOkayButtonClicked.emit();
  // }
}
